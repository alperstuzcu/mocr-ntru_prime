#pragma once
#include <gmp.h>


// Use the Pollards-Rho method to factorize 'pq'. Assume that 'pq' is the product of two primes.
// Store the found factorization into 'p' and 'q'.
// Return 1 when succeeding and 0 otherwise.
int pollards_rho(const mpz_t pq, mpz_t p, mpz_t q, gmp_randstate_t rng);
