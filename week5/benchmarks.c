#pragma once
#include <stdio.h>
#include <gmp.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>

#include "trial_division.h"
#include "pollards_rho.h"

#define BASE_10 10
#define BASE_2 2
#define PRIME_TEST_COUNT 10
#define N2Q_TIME 0.000001
#define INF_LOOP 1

#define STYLE_BOLD_PY  "\"\\033[1m\""
#define STYLE_AZURE_PY "\"\\033[38;2;0;150;255m\""
#define STYLE_AMBER_PY "\"\\033[38;2;255;205;0m\""
#define STYLE_RED_PY   "\"\\033[38;2;255;0;0m\""
#define STYLE_RESET_PY "\"\\033[0m\""


unsigned int ceil_div(const unsigned int n, const unsigned int m) {
	return (n + m - 1u) / m;
}


// Returns a big integer that is the product of two large pseudo primes, where the bit count of the product is 'n'.
// This function works properly when 'n' is 8 or greater.
void get_pseudo_prime_prod(const unsigned int n, mpz_t pq, int jump) {
	unsigned int p_power = n / 2u - 1u + (n & 1u);

	// Initialize integers.
	mpz_t p;
	mpz_t q;
	mpz_init(p);
	mpz_init(q);

	// Define p and q as two unique pseudo primes.
	mpz_ui_pow_ui(p, 2u, p_power);
	if (!(n & 1)) {
		mpz_ui_pow_ui(q, 2u, p_power - 1u);
		mpz_add(p, p, q);
	}
	for (int i = 0; i < jump + 1; i++)
		mpz_nextprime(p, p);
	mpz_nextprime(q, p);

	// Factor p and q.
	mpz_mul(pq, p, q);

	// Clear memory.
	mpz_clear(p);
	mpz_clear(q);
}


// Get time in seconds since the beginning of time.
unsigned long long int get_time() {
	struct timeval time_cur;
	gettimeofday(&time_cur, NULL);
	return time_cur.tv_sec * 1000000ull + time_cur.tv_usec;
}


// Return the delta time between 'time_start' and 'time_end' as a float.
float get_time_delta(const unsigned long long int time_start, const unsigned long long int time_end) {
	return (time_end - time_start) * 0.000001f;
}


// Find the upper limit for bit count of the factoring function for the given time 'limit_t' and return it.
unsigned int find_limit(const float limit_t, int (*factor_func)(const mpz_t, mpz_t, mpz_t, gmp_randstate_t), gmp_randstate_t rng) {
	// This search uses linear probing.
	// It checks for every bit size how long factoring takes and stops when factoring takes longer than 'limit_t'.
	// The output is the last parameter which did not exceed the runtime limit.
	for (unsigned int n = 8; INF_LOOP; n++) {
		mpz_t p;
		mpz_t q;
		mpz_t pq;
		mpz_init(p);
		mpz_init(q);
		mpz_init(pq);
		int succ = 0;
		unsigned long long int time_s, time_t;

		// Attempt to factor N until we succeed.
		int i = 0;
		while (!succ) {
			// Get some arbitrary prime factor for the benchmark.
			get_pseudo_prime_prod(n, pq, i++);

			// Factor and measure the time.
			time_s = get_time();
			succ = factor_func(pq, p, q, rng);
			time_t = get_time();
		}

		// Print result to make sure the test isn't optimized away by the compiler.
		printf("print(\"n =\"" STYLE_BOLD_PY STYLE_AZURE_PY ", %d, " STYLE_RESET_PY ", ", n);
		mpz_out_str(stdout, BASE_10, pq);
		printf(", \"factored into:\", ");
		mpz_out_str(stdout, BASE_10, p);
		printf(", ");
		mpz_out_str(stdout, BASE_10, q);
		printf(")\n");

		if (get_time_delta(time_s, time_t) > limit_t)
			return n - 1u;
	}
}


// Time how long it takes to factor an 'n'-bit integer with 'sample_size' samples.
// 'factor_func' is the factorization algorithm.
double time_with_samples(const unsigned int n, const unsigned int sample_size, int (*factor_func)(const mpz_t, mpz_t, mpz_t, gmp_randstate_t), gmp_randstate_t rng) {
	mpz_t p;
	mpz_t q;
	mpz_t pq;
	mpz_init(p);
	mpz_init(q);
	mpz_init(pq);
	int succ = 0;
	unsigned long long int time_s, time_t;

	// Attempt to factor N until we succeed.
	int i = 0;
	while (!succ) {
		// Get some arbitrary prime factor for the benchmark.
		get_pseudo_prime_prod(n, pq, i++);

		// Factor and measure the time.
		time_s = get_time();
		for (int j = 0; j < sample_size; j++)
			succ = factor_func(pq, p, q, rng);
		time_t = get_time();
	}

	// Free bigints.
	mpz_clear(p);
	mpz_clear(q);
	mpz_clear(pq);

	return get_time_delta(time_s, time_t) / (double)sample_size;
}

// Output the results (python code) for exercise a.
void exercise_a(gmp_randstate_t rng) {
	// Define the limits
	float limits[] = { 0.1f, 10.f, 600.f };

	for (int i = 0; i < 3; i++) {
		printf("print(" STYLE_AMBER_PY STYLE_BOLD_PY "\"\\n\\nFactoring with trial division up to\", %f, \"seconds\"" STYLE_RESET_PY ")\n", limits[i]);
		find_limit(limits[i], &trial_division, rng);

		printf("print(" STYLE_AMBER_PY STYLE_BOLD_PY "\"\\n\\nFactoring with Pollard's Rho up to\", %f, \"seconds\"" STYLE_RESET_PY ")\n", limits[i]);
		find_limit(limits[i], &pollards_rho, rng);
	}
}


// Output the results (python code) for exercise b.
void exercise_b(gmp_randstate_t rng) {
	// Parameters for the plot.
	unsigned int n_start = 8u;
	unsigned int n_end = 20u;
	unsigned int sample_size = 1000000u;

	
	double td_times[n_end - n_start + 1];
	double pr_times[n_end - n_start + 1];
	int i = 0;
	for (int n = n_start; n <= n_end; n++) {
		td_times[i] = time_with_samples(n, sample_size, &trial_division, rng);
		pr_times[i] = time_with_samples(n, sample_size, &pollards_rho, rng);
		i++;
	}		
	
	printf("import matplotlib.pyplot as plt\n");

	// Print the array for n.
	printf("n = [");
	for (int n = n_start; n <= n_end; n++) {
		printf("%d", n);
		if (n < n_end)
			printf(", ");
	}
	printf("]\n");

	// Print the array for the trial division times.
	char buf[100];
	printf("td = [");
	for (int n = n_start; n <= n_end; n++) {
		gcvt(td_times[n - n_start], 16, buf);
		printf("%s", buf);
		if (n < n_end)
			printf(", ");
	}
	printf("]\n");
	
	// Print the array for Pollard's Rho times.
	printf("pr = [");
	for (int n = n_start; n <= n_end; n++) {
		gcvt(pr_times[n - n_start], 16, buf);
		printf("%s", buf);
		if (n < n_end)
			printf(", ");
	}
	printf("]\n");

	// Output matplotlib commands to plot the data.
	printf("plt.figure()\n");
	printf("plt.plot(n, td, label='trial division')\n");
	printf("plt.plot(n, pr, label='pollard\\'s rho')\n");
	printf("plt.legend()\n");
	printf("plt.xlabel('number of bits in N')\n");
	printf("plt.ylabel('time (s)')\n");
	printf("plt.show()\n");

}


int main(int argc, char** argv) {
	// Parse input.
	if (argc < 2) {
		// Display help and quit.
		printf("print(" STYLE_BOLD_PY STYLE_AMBER_PY "\"Use like...\"" STYLE_RESET_PY "\"\\n%s <assignment_letter> | python3\")\n", argv[0]);
		return 0;
	}

	// init the rng
	gmp_randstate_t rng;
	gmp_randinit_default(rng);

	if (argv[1][0] == 'a' || argv[1][0] == 'A')
		exercise_a(rng);
	else if (argv[1][0] == 'b' || argv[1][0] == 'B')
		exercise_b(rng);

	return 0;
}
