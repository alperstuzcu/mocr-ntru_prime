#include "pollards_rho.h"


void f(mpz_t x, const mpz_t pq) {
	mpz_mul(x, x, x);
	mpz_add_ui(x, x, 1u);
	mpz_mod(x, x, pq);
}


int pollards_rho(const mpz_t pq, mpz_t p, mpz_t q, gmp_randstate_t rng) {
	// Fix x and x'
	mpz_t x;
	mpz_t x_prime;
	mpz_init(x);
	mpz_init(x_prime);

	// Get a random sample.
	mpz_urandomm(x, rng, pq);
	mpz_set(x_prime, x);

	// Loop until the square root.
	mpz_t root;
	mpz_t i;
	mpz_t gcd;
	mpz_t x_delta;
	mpz_init(root);
	mpz_init(i);
	mpz_init(gcd);
	mpz_init(x_delta);
	mpz_root(root, pq, 2);
	for (mpz_set_ui(i, 0); mpz_cmp(i, root) < 0; mpz_add_ui(i, i, 1)) {
		f(x, pq);
		f(x_prime, pq);
		f(x_prime, pq);
		mpz_sub(x_delta, x, x_prime);
		mpz_gcd(gcd, x_delta, pq);
		if (mpz_cmp_ui(gcd, 1u)) {
			// Return the prime factors.
			mpz_set(p, gcd);
			mpz_div(q, pq, gcd);

			// Clear bigints.
			mpz_clear(x);
			mpz_clear(x_prime);
			mpz_clear(root);
			mpz_clear(i);
			mpz_clear(gcd);
			mpz_clear(x_delta);
			return 1;
		}
	}
	
	// Clear bigints.
	mpz_clear(x);
	mpz_clear(x_prime);
	mpz_clear(root);
	mpz_clear(i);
	mpz_clear(gcd);
	mpz_clear(x_delta);

	return 0;
}
