# Requirements
The GMP library for big integers in c.
Install with the command (Ubuntu):
```
sudo apt install libgmp-dev
```

# Files
* `Makefile`, build script
* `benchmarks.c`, implementation of the automated benchmarks. It also contains the N generater which are used for the tests.
* `pollards_rho.*`, Implementation of the Pollard's Rho algorithm.
* `result_a.py`, The resulting output when running the code for exercises a.
* `result_b.py`, The resulting output when running the code for exercise b.
* `trial_division.*`, Implementation of the trial division algorithm.

# Run
###### exercise a
Run with:
```
make a
```
Please note that this code can run for hours. If you wish to just see the output of this code, please run:
```
python3 result_a.py
```

###### exercise b
Run with:
```
make b
```
Please note that this code can run for a couple minutes. If you wish to just see the output of this code, please run:
```
python3 result_b.py
```
