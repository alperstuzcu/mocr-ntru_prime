#include <gmp.h>


// Factor 'pq', which is assumed to be a factor of p prime and q prime,
// and store the prime factors into 'p' and 'q'.
// Always return 1, because this exhaustive search always succeeds.
int trial_division(const mpz_t pq, mpz_t p, mpz_t q, gmp_randstate_t rng);
