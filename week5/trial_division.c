#include "trial_division.h"


int trial_division(const mpz_t pq, mpz_t p, mpz_t q, gmp_randstate_t rng) {
	mpz_t i;
	mpz_init(i);
	for (mpz_set_ui(i, 2u); mpz_cmp(i, pq) < 0; mpz_add_ui(i, i, 1u)) {
		if (mpz_divisible_p(pq, i)) {
			mpz_div(p, pq, i);
			mpz_set(q, i);

			mpz_clear(i);

			return 1;
		}
	}
}
