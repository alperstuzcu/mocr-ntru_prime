# Assignment
Define an attack that can forge a valid tag by using a MAC oracle. The file `attack.c`
contains our implementation of the attack. The file `message.txt` defines the message
of which we wish to forge a tag. The rest of the files are part of the framework
used to connect and communicate with the oracle.

# How to run
Compile:
```
make attack
```
Run:
```
./attack <message file>
```
Run with the message from the assignment:
```
./attack message.txt
```