#include "oracle.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


#define MAX_LENGTH 512
#define BLOCK_SIZE 16
#define STYLE_BOLD "\033[1m"
#define STYLE_AZURE "\033[38;2;0;150;255m"
#define STYLE_AMBER "\033[38;2;255;205;0m"
#define STYLE_RED   "\033[38;2;255;0;0m"
#define STYLE_RESET "\033[0m"


int main(int argc, char *argv[]) {
    // open and parse file
    unsigned char message[MAX_LENGTH];
    unsigned char tag[BLOCK_SIZE];
    int mlength, ret;
    FILE *fpIn;

    if (argc != 2) {
        printf("Usage: sample <filename>\n");
        return -1;
    }

    fpIn = fopen(argv[1], "r");
    if (!fpIn) {
        printf(STYLE_BOLD STYLE_RED "File does not exist!\n" STYLE_RESET);
        return -1;
    }
    for(int i = 0; i < MAX_LENGTH; i++) {
      if (fscanf(fpIn, "%c", &message[i]) == EOF) {
          mlength = i;   
          break;
       }
    }
    fclose(fpIn);

    // print the message
    puts(STYLE_BOLD STYLE_AZURE "The message:" STYLE_RESET);
    for (int i = 0; i < mlength; i++)
        putchar(message[i]);
    putchar('\n');
    putchar('\n');

    // connect
    printf(STYLE_BOLD STYLE_AMBER);
    Oracle_Connect();
    printf(STYLE_RESET "\n");

    // forge tag
    int block_count = mlength / BLOCK_SIZE;
    char t_forge[BLOCK_SIZE]; 
    memset(t_forge, '\000', BLOCK_SIZE); 
    for (int i = 0; i < block_count; i += 2) {
        char q[2 * BLOCK_SIZE];
        memcpy(q, message + BLOCK_SIZE * i, 2 * BLOCK_SIZE);
        
        // chain this query with the previous tag
        for (int j = 0; j < BLOCK_SIZE; j++)
            q[j] ^= t_forge[j];
        // send the query and update the tag
        Mac(q, 2 * BLOCK_SIZE, t_forge); 
    }

    // print the tag
    puts(STYLE_BOLD STYLE_AZURE "The forged tag:" STYLE_RESET);
    for (int i = 0; i < BLOCK_SIZE; i++)
        printf("%02x", (unsigned char)t_forge[i]);
    putchar('\n');
    putchar('\n');
    
    // verify the result
    ret = Vrfy(message, mlength, t_forge);
    if (ret == 1)
        puts(STYLE_BOLD STYLE_AMBER "Tag forged successfully!\n" STYLE_RESET);
    else
        puts(STYLE_BOLD STYLE_RED "Tag forging failed.\n" STYLE_RESET);

    // disconnect and close program
    printf(STYLE_BOLD STYLE_AMBER);
    Oracle_Disconnect();
    printf(STYLE_RESET);
}


















