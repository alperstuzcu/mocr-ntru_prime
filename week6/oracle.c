#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "oracle.h"
#include "style.h"


int oracle_connect(int* sign_fd, int* verify_fd) {
	struct sockaddr_in sign_servaddr, verify_servaddr;
	
	*sign_fd = socket(AF_INET, SOCK_STREAM, 0);
	*verify_fd = socket(AF_INET, SOCK_STREAM, 0);

	bzero(&sign_servaddr, sizeof(sign_servaddr));
	bzero(&verify_servaddr, sizeof(verify_servaddr));

	sign_servaddr.sin_family = AF_INET;
	sign_servaddr.sin_addr.s_addr = inet_addr("128.8.130.16");
	sign_servaddr.sin_port = htons(49104);
	
	verify_servaddr.sin_family = AF_INET;
	verify_servaddr.sin_addr.s_addr = inet_addr("128.8.130.16");
	verify_servaddr.sin_port = htons(49105);
	
	if (!connect(*sign_fd, (struct sockaddr *)&sign_servaddr, sizeof(sign_servaddr)) &&
		!connect(*verify_fd, (struct sockaddr *)&verify_servaddr, sizeof(verify_servaddr)) ) {
		printf(STYLE_BOLD STYLE_GREEN "Connected to server successfully.\n" STYLE_RESET);
		return 0;
	} else {
		perror(STYLE_BOLD STYLE_RED "Failed to connect to oracle" STYLE_RESET);
		return -1;
	}
}


int oracle_disconnect(int sign_fd, int verify_fd) {
	if (!close(sign_fd) && !close(verify_fd)) {
		printf(STYLE_BOLD STYLE_GREEN "Connection closed successfully.\n" STYLE_RESET);
		return 0;
	} else {
		perror(STYLE_BOLD STYLE_RED "[WARNING]: You haven't connected to the server yet" STYLE_RESET);
		return -1;
	}
}


int sign(const mpz_t* msg, mpz_t sigma, int sign_fd) {
	// Convert the message to a packet.
	// A packet contains the message as a string in base 2 with 'X' concatenated at the end.
	unsigned char buf[BUFFER_SIZE];
	unsigned char buf_in[BUFFER_SIZE];
	mpz_get_str(buf, BASE_2, *msg);
	unsigned int str_size = strlen(buf);
	buf[str_size] = 'X';

	// Interact with the oracle.
	send(sign_fd, buf, str_size + 1u, NOFLAGS);
	unsigned int str_size_in = recv(sign_fd, buf_in, BUFFER_SIZE, NOFLAGS);
	
	// Convert the packet to a signature.
	buf_in[str_size_in] = '\n';
	mpz_set_str(sigma, buf_in, BASE_2);

	return 0;
}


int verify(const mpz_t* msg, const mpz_t* sigma, int verify_fd) {
	// Convert the message into a packet.
	// The packet is as follows: <base2 string of msg> || ":" || <base2 string of sigma> || "X"
	unsigned char buf_out[BUFFER_SIZE];
	unsigned char buf_in[BUFFER_SIZE];
	mpz_get_str(buf_out, BASE_2, *msg);
	unsigned int packet_out_size = strlen(buf_out);
	buf_out[packet_out_size++] = ':';
	mpz_get_str(buf_out + packet_out_size, BASE_2, *sigma);
	packet_out_size = strlen(buf_out);
	buf_out[packet_out_size++] = 'X';

	// Interact with the oracle.
	send(verify_fd, buf_out, packet_out_size, NOFLAGS);
	unsigned int str_size_in = recv(verify_fd, buf_in, BUFFER_SIZE, NOFLAGS);

	// Parse the result.
	buf_in[str_size_in] = '\0';
	return buf_in[0] == '1' ;
}


void ascii2bigint(const char* const msg, mpz_t msg_int) {
	mpz_t inter;
	mpz_init(inter);
	mpz_set_ui(msg_int, 0);
	unsigned int msg_size = strlen(msg);

	// Store the message string in big-endian.	
	for (unsigned int i = 0; i < msg_size; i++) {
		mpz_ui_pow_ui(inter, 2u, 8u * (msg_size - 1u - i));
		mpz_mul_ui(inter, inter, (unsigned int)msg[i]);
		mpz_add(msg_int, msg_int, inter);
	}

	mpz_clear(inter);
}
