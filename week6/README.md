# Prerequisites
This assignment implementation uses the GMP library for big integers.
Download it like so (Ubuntu):
```
sudo apt install libgmp-dev
```

# Files
* `oracle.*`, Framework used to interact with the oracle.
* `style.h`, Definitions of macros used for stylish stdout formatting.
* `attack.c`, Implementation of the attack as described in the assignment.
* `Makefile`, Build script.

# How to compile
Run the command:
```
make attack
```

# How to run
Run with the message of the assignment:
```
./attack.out
```

Run with any arbitrary message:
```
./attack.out <message>
```
