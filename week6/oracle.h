#include <gmp.h>


// Constants for the program
#define BASE_10 10u
#define BASE_2 2u
#define BASE_16 16u
#define BUFFER_SIZE 8192u
#define NOFLAGS 0

int oracle_connect(int* sign_fd, int* verify_fd);
int oracle_disconnect(int sign_fd, int verify_fd);

int sign(const mpz_t* msg, mpz_t sigma, int sign_fd);
int verify(const mpz_t* msg, const mpz_t* sigma, int verify_fd);

void ascii2bigint(const char* msg, mpz_t msg_int);
