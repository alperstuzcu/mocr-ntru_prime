#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>

#include "style.h"
#include "oracle.h"


// Try to find a trivial factoring for 'n' and store the factors in 'z0' and 'z1'.
// Return 1 on success and 0 otherwise.
int factorize(const mpz_t* n, mpz_t z0, mpz_t z1) {
	mpz_set_ui(z0, 2u);
	for (int i = 0; i < 1000000u; i++) {
		if (mpz_divisible_p(*n, z0)) {
			mpz_fdiv_q(z1, *n, z0);
			return 1;
		}
		mpz_add_ui(z0, z0, 1u);
		if (!mpz_cmp(z0, *n))
			break;
	}
	return 0;
}


int main(int argc, char** argv) {
	// Load the default parameter.
	const char msg_default[] = "Crypto is hard --- even schemes that look complex can be broken";
	const char* msg_str = msg_default;

	// Parse the input if applicable.
	if (argc > 1)
		msg_str = (const char*)argv[1];

	// Connect to the oracle.
	int sign_fd, verify_fd;
	oracle_connect(&sign_fd, &verify_fd);

	// Initialize key and message.
	mpz_t N;
	mpz_init(N);
	mpz_set_str(N, "a99263f5cd9a6c3d93411fbf682859a07b5e41c38abade2a551798e6c8af5af08dee5c7420c99f0f3372e8f2bfc4d0c85115b45a0abc540349bf08b251a80b85975214248dffe57095248d1c7e375125c1da25227926c99a5ba4432dfcfdae300b795f1764af043e7c1a8e070f5229a4cbc6c5680ff2cd6fa1d62d39faf3d41d", BASE_16);
	mpz_t e;
	mpz_init(e);
	mpz_set_ui(e, 65537u);

	// Factorize the message into two trivial factors and querry the signatures of those factors.
	mpz_t msg;
	mpz_init(msg);
	ascii2bigint(msg_str, msg);
	mpz_t z0, z1;
	mpz_init(z0);
	mpz_init(z1);
	int can_factor = factorize(&msg, z0, z1);
	if (!can_factor) {
		printf(STYLE_BOLD STYLE_RED "FAILED TO FACTORIZE MESSAGE\n" STYLE_RESET);
		return 0;
	}
	mpz_t one;
	mpz_init(one);
	mpz_set_ui(one, 1u);

	// Now get the signatures of the two bits.
	mpz_t sigma_0, sigma_1, sigma_one;
	mpz_init(sigma_0);
	mpz_init(sigma_1);
	mpz_init(sigma_one);
	
	sign(&z0, sigma_0, sign_fd);
	sign(&z1, sigma_1, sign_fd);
	sign(&one, sigma_one, sign_fd);

	// Combine the results s.t. we get the signature of the message.
	mpz_t one_inv, dont_care, sigma;
	mpz_init(one_inv);
	mpz_init(dont_care);
	mpz_init(sigma);
	
	mpz_gcdext(dont_care, dont_care, one_inv, N, sigma_one);
	mpz_mod(one_inv, one_inv, N);
	mpz_mul(sigma, sigma_0, sigma_1);
	mpz_mul(sigma, sigma, one_inv);
	mpz_mod(sigma, sigma, N);

	// Print the message and corresponding signature.
	printf("The message is:\n" STYLE_BOLD STYLE_ORANGE "%s\n\n" STYLE_RESET, msg_str);
	printf("The signature is:\n" STYLE_BOLD STYLE_BLUE);
	mpz_out_str(stdout, BASE_16, sigma);
	printf("\n\n" STYLE_RESET);

	// Test the signature.
	int succ = verify(&msg, &sigma, verify_fd);
	if (succ)
		printf(STYLE_BOLD STYLE_GREEN "SUCCESS\n\n" STYLE_RESET);	
	else
		printf(STYLE_BOLD STYLE_RED "FAILURE\n\n" STYLE_RESET);

	// Disconnect from oracle.
	oracle_disconnect(sign_fd, verify_fd);
}
